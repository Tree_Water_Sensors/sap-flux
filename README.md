# Sapflux sensor system

This is a snapshot of where this project stands currently and notes on further directions.

## Background

This is a sensor system I've started developing based on the description by Vandegehuchte and Steppe in the paper ["Sapflow+, a four-needle heat-pulse sap flow sensor enabling nonempirical sap flux density and water content measurements"](http://onlinelibrary.wiley.com/doi/10.1111/j.1469-8137.2012.04237.x/full) and my previous work with [compost monitoring systems](http://www.instructables.com/id/Compost-Sensor/) and [dendrometry](https://gitlab.com/Tree_Water_Sensors/dendrometer). This project is still in the testing and development stage and there is a lot of testing that needs to happen before it can be confidently deployed in the field. There are several bugs which need to be fixed and some specific areas that need further testing and development.

## The System

This sensor system is made up of multiple wirelessly connected sensors installed within a research site up to around a kilometer in size (depending on vegetation type and density, etc). The sensors communicate wirelessly with a datalogger over a low power radio connection. Each sensor is individually powered and will take readings at a hardcoded interval.

## The Sensor

The sensor is made up of three major components. The housing is designed to be 3d printed on a hobbiest grade FDM machine such as Makerbot or Lulzbot. The sensor needles are three custom fabricated thermocouple based temperature sensors and a resistive heater needle. The circuit is custom and needs to be manufactured at a PCB manufacturing facility ([EasyEDA](https://easyeda.com/order) or [OSHPark](https://oshpark.com/) are good inexpensive resources) and assembled. All parts and designs are documented in this repository.

[Assembly instructions here](/docu/build.md)

### Housing

![](/docu/img/housingRender.png){width=50%}

The housing is a two piece design which can be 3d printed without any supports. A medium quality setting should be fine on most machines and materials.

The housing should be printed from a light colored material to reduce thermal absorbtion, and out of a material that can withstand outside environments and UV radiation. I have had very good luck with the [nGen filament by colorFabb](https://www.lulzbot.com/store/filament/ngen). Waterproofing each printed piece with an exterior grade paint would not be a bad idea because most 3d printing materials do not create watertight parts and I have had leakage problems in the past.


### Sensor Needles

![](/docu/img/ndle-1.jpg)

The temperature sensors and heater are built inside of 19 gauge stainless steel dispensing needles. They snap-fit into the housing and are held in place with retention tabs built into the housing body.

### Circuit Overview

![](/docu/img/pcb-1.jpg)

The design files for the PCB and a bill of materials are included in the repository. If you are assembling your own boards, please note that while most of the parts are 0603, there are several 0.5mm pitch DFN and QFN parts which can be difficult to work with. The board is also designed around a [TagConnect pogo-pin programming cable](https://macrofab.com/blog/designing-pogo-pin-programming-cables-intro-tag-connect/).


### Other Files

There is a drill jig in the design files to assist in drilling the installation holes in tree. The jig is designed to be used in conjunction with small steel screws that have had their centers bored out to the diameter of the drill bit used. This will require some extra machining, but is quick and easy on a lathe. The spacing of the included jig is 7mm, which matches the housing spacing. Both can be modified to accommodate different spacings.

## Datalogger

There is no custom datalogger designed to go with these sensors, but one can be constructed with off the shelf parts and a basic understanding of the Arduino ecosystem. There are excellent resources at [Sparkfun.com](http://www.sparkfun.com) to help you get started.

### Datalogger Construction

![](/docu/img/dlsys.png)

The datalogger has 4 main components to it.
* A microcontroller, which processes all the code.
* A lower power radio, which communicates with the sensors.
* An SD Card, to store the data on.
* A Clock, to give accurate time stamps to the data as they arrive.

I would recomend the following parts

* Microcontroller and Radio combination: [Moteino](https://lowpowerlab.com/guide/moteino/)
  * [Anarduino](http://www.anarduino.com/miniwireless/) is also an option, but doens't have as good of support.
* SD Card: [Transflash Breakout](https://www.sparkfun.com/products/544)
* Clock: [Real Time Clock](https://www.adafruit.com/product/3013)

That's it! You'll probably want an external antenna and antenna connector, but you can find documentation on how to that on the Moteino website.

## Known Issues

### Circuit Design

The biggest problem is that the driver circuit for the Heater generates heat which effects the reading of the thermocouple temperatures. The driver creates a pulse of heat within the circuit board which changes the temperature of the thermocouple to PCB connection faster than the internal temperature sensor within the thermocouple amplifiers can compensate for. The result is false readings from the temperature sensors, which is bad. The thermocouple amplifiers respond well to slow changes of ambient temperatures, but rapid changes can cause problems. The solution to this is to thermally isolate the heater driver, and/or to move the thermocouple connections closer to the amplifier IC or use an external temperature reference at the solder connection for cold junction compensation. More information about this is in the build tutorial.

The second issue is with the power trace between the positive battery terminal and the 3.3volt regulator. The heater driver draws several amps when its on, and in version 20 of the pcb there was an error in trace width, which led to large voltage drops and system instability. This can be fixed easily by soldering a piece of wire between the battery connector and the heater driver.
