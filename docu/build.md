### Sap Flux Build Document

This is documentation on the construction of a single sap flux sensor.

## Notes

There are a couple of minor bugs in the PCB that should be acknowledged before moving forward.

The circuit that drives the heater generates heat when it operates. This can cause false readings while taking the thermocouple readings due to innacurate cold junction compensation. Because of this, we have to do a couple delicate procedures to the thermocouples on installation (documented below). Also, a jumper wire must be soldered between the positive terminal of the battery connector and the power input of the 3.3v voltage regulator (also documented below).


## Table of Contents

- [Parts Required](#parts-required)
- [Tools Required](#tools-required)
- [Prep PCB](#prep-pcb)
- [Make Thermocouples](#make-thermocouples)
- [Assemble Needles](#prepare-the-sensor-needles)
- [Make Heaters](#prepare-heater-needle)
- [Assembly](#assembly)
- [Troubleshooting](#troubleshooting)

## Parts Required

- Assembled sensor PCB
  - design files in [/sensor/pcb](/sensor/pcb/)
- Housing (2 pieces)
  - design files in [/sensor/mech/housing](/sensor/mech/housing/)
- 38mm long 19gauge Luer Lock dispensing Needles
  - [Zephtronics](http://www.zeph.com/applicator_tips.htm) part: ZT-5-019-15-L
- [Thermocouple Wire](http://www.omega.com/pptst/TFIR_CH_CI_CC_CY_AL.html) (about 15 inches each)
  - 0.005in dia PFA insulated Constantan: TFCC-005-(\*)
  - 0.005in dia PFA insulated Copper: TFCP-005-(\*)
- small amount of ~24awg stranded wire
- 10cm of 0.005in dia Constantan wire
- [3.5w Solar Panel](https://www.digikey.com/products/en/sensors-transducers/solar-cells/514?k=&pkeyword=&pv1256=319&FV=ffe00202&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=0&ptm=0&fid=0&pageSize=25)
- 18650 lithium battery with protection circuitry of at least 2800mAh capacity
- Stainless Screws x3: [Something similar to this](https://www.mcmaster.com/#92470a018/=17xg4uk)

## Tools Required

- misc tools
  - wire strippers
  - cutters
  - soldering iron
  - small screw driver
  - etc...
- super glue
- electrical tape
- tweasers

---------

## Prep PCB

Due to an error in the PCB, there must be a jumper wire installed between the positive terminal of the battery connector and the power feed for the 3.3v regulator. If you don't do this, interence caused by the Heater Driver circuit will cause the system to enter a boot loop and fail.

Take a razor knife and scrape away the green solder mask from the PCB trace under 3.3v regulator circuit. Solder a wire from there to the positive terminal of the battery connector. Tinning the connector first and using a hot iron will help the solder adhere to it.

![](/docu/img/sfTut-23.jpg)

## Make Thermocouples

![](/docu/img/sfTut-18.jpg)

The thermocouples are Type T (Constantan/Copper) and very small, which presents some challenges.

![](/docu/img/sfTut-16.jpg)

The copper is Blue, the constantan is Red. You'll need 3 pieces of Blue about 1 inch long and 3 pieces about 4.5 inches long, and 3 pieces of Red about 3.5 inches long.
Strip about 0.125inches off insulation off each end. This can be tricky due to the size of the wire. I found that pinching the wire with a pair of (small and sharp) diagonal cutters with just enough force to cut the insulation and pulling sharply towards the end of the wire while firmly pinching the long end in my fingers worked well. Experiment on some scrap pieces for what works best for you.

Solder together the two long wires (one Red, one Blue), and then solder the short Blue wire onto the free end of the Red wire, like so. You should end up with three complete thermocouples that look like this.

![](/docu/img/sfTut-15.jpg)

The ends should be twisted together before soldering to make a clean strong joint. Using tweasers and not drinking coffee helps. If you have access to a thermocouple welder, that is also a good option. You should end up with something looking like this at each junction.

![](/docu/img/sfTut-14.jpg)

## Prepare the Sensor Needles

The needles that we will be inserting the Thermocouples into need a little bit of prep.

Take a pair of large diagonal cutters, and clip off the top of the plastic Luer Lock attachment. Do this to all 4 needles that you'll use for a single sensor. Clip the tops approximately in half, see picture for reference. Depending on the quality of the 3d printed housings, clipping off the little wings on the side could be helpful (not shown).

![](/docu/img/sfTut-17.jpg)

Install the thermocouples inside of the needles so that the junction of the long wires is 20mm from the end of the needle, and super glue them into place. Once dry, test with a multimeter to make sure there is no electrical continuity between the thermocouple wires and the metal wall of the needle. You should have 3 needles with the thermocouple wires sticking out the top, similar to this (note: short Blue wire not pictured).

![](/docu/img/sfTut-13.jpg)



## Prepare Heater Needle

We will be making a resistive heater with the fourth needle.

The Constantan (Red) wire has a resistance of about 1 ohm per inch. We'll be making an 8 ohm heater by folding this length of wire inside of the needle. Cut 8 inches of wire and strip both ends as done in the previous step.

Fold the wire in half:

![](/docu/img/sfTut-9.jpg)

Insert the folded end into the top of the needle and thread it through until it comes out at the tip. Pull it through until just a little bit of the two ends are showing at the top of the needle. Fold the wires a second time so that the distance from this second fold to the first fold you made is the length of the needle (see below).

![](/docu/img/sfTut-7.jpg)

Make a crease in the wire at that second fold and carefully feed the first fold into the bottom of the needle. Make sure there are no twists in the four wires, or it will not fit inside of the needle. Carefully feed all the wires into the needle until there is a small loop at the end.

![](/docu/img/sfTut-5.jpg)

Grasp the two wires at the top of the needle, and carefully pull until that loop of wire at the tip of the needle is pulled into the needle. Seal the end with a drop of super glue.

Measure the resistance of the heater. It should be 8 ohms. If it is more, trim some of the wire until it is exactly 8 ohms (or just make a note of the resistance for future calculations). Solder some stranded hookup wire onto the ends of the wire and protect the connections with heat shrink. Do this carefully.


## Assembly

### install the needles

Take the three thermocouple needles and press them into the 3d printed housing like this:

![](/docu/img/sfTut-12.jpg)

![](/docu/img/sfTut-10.jpg)

![](/docu/img/sfTut-11.jpg)

Do the same with the Heater. The final product should look something like this, with the heater in the middle, and themocouples on three sides.

![](/docu/img/sfTut-3.jpg)

### connect them to the pcb

This part is tricky. Due to the problem with the heater driver generating heat in the PCB, we have to attach the junction of the Red and short Blue wire to the thermcouple amplifier IC.
Look carefully at this image and I will try to explain in words:

![](/docu/img/sfTut-2.jpg)

The solderpads for the Thermcouples are small and next to their respective holes in the PCB. One is round, and the other is square. The long blue wire attaches to the round pad, the short blue wire attaches to the square pad, and the Red/Short Blue junction must be glued to the top of the respective IC. I did it by putting a drop of super glue on top of the IC, and then taping the thermocouple junction onto it. It worked well, but it is a brittle connection. Any thermally conductive glue would work.

After the thermocouples, solder the heater to the PCB to the appropriate solder pads next to the center hole, the polarity does not matter (not pictured).

### install the pcb

Turn the PCB over so that the battery connector is facing up, and use some small stainless screws to attach it to the housing. Be gentle and make sure no wires get pinched between the PCB and the housing, and that they are secure and protected.

![](/docu/img/sfTut-1.jpg)

## That's it!

Upload the code, and test to make sure all the thermocouples are giving good values.

## Troubleshooting

- If you're getting bad values, it will either be a bad connection in that thermocouple, or the junction is shorted to the side of the needle. Toss that needle and try again, once they are glued in place there is no taking them out.
- It can be difficult to remove the needles from the housing once installed. To do it, use a small screwdriver to pry the plastic edges of the needle away from the retention tabs on the housing, and use gentle pressure to for the needle back out of the housing.
- Getting all the wires of the heater into the needle can be difficult. It is imperetive that there are no twists in the wires as they are fed into the needle. Also thermal grease can help lubricate the process.
- It may be a good idea to dab the tips of the thermocouples in super glue to insulate the junction prior to inserting them into the needles.
- If constructing the heater needles is too difficult, upping the size of the needle to 18 or 17 gauge probably won't hurt anything.
